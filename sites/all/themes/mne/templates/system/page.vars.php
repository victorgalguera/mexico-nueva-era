<?php
/**
 * @file
 * page.vars.php
 */

/**
 * Implements hook_preprocess_page().
 *
 * @see page.tpl.php
 */
function mne_preprocess_page(&$variables) {
  // Add information about the number of sidebars.
  if (!empty($variables['page']['sidebar_first']) && !empty($variables['page']['sidebar_second'])) {
    $variables['content_column_class'] = ' class="col-sm-7"';
    $variables['container_column_class_body'] = ' class="col-sm-9"';
    $variables['container_column_class_sidebar'] = ' class="col-sm-3"';
  }
  elseif (!empty($variables['page']['sidebar_first'])) {
    $variables['content_column_class'] = ' class="col-sm-12"';
    $variables['container_column_class_body'] = ' class="col-sm-12"';
    $variables['container_column_class_sidebar'] = ' class="col-sm-4"';
  }
  elseif (!empty($variables['page']['sidebar_second'])) {
    $variables['content_column_class'] = ' class="col-sm-12"';
    $variables['container_column_class_body'] = ' class="col-md-8 col-sm-12"';
    $variables['container_column_class_sidebar'] = ' class="col-md-4 col-sm-12"';
  }
  else {
    $variables['content_column_class'] = ' class="col-sm-12"';
    $variables['container_column_class_body'] = ' class="col-sm-12"';
    //$variables['container_column_class_sidebar'] = ' class="col-sm-4"';
  }

  // Primary nav.
  $variables['primary_nav'] = FALSE;
  if ($variables['main_menu']) {
    // Build links.
    $variables['primary_nav'] = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
    // Provide default theme wrapper function.
    $variables['primary_nav']['#theme_wrappers'] = array('menu_tree__primary');
  }

  // Secondary nav.
  $variables['secondary_nav'] = FALSE;
  if ($variables['secondary_menu']) {
    // Build links.
    $variables['secondary_nav'] = menu_tree(variable_get('menu_secondary_links_source', 'user-menu'));
    // Provide default theme wrapper function.
    $variables['secondary_nav']['#theme_wrappers'] = array('menu_tree__secondary');
  }

  $variables['navbar_classes_array'] = array('navbar');

  if (theme_get_setting('bootstrap_navbar_position') !== '') {
    $variables['navbar_classes_array'][] = 'navbar-' . theme_get_setting('bootstrap_navbar_position');
  }
  else {
    $variables['navbar_classes_array'][] = '';
  }
  if (theme_get_setting('bootstrap_navbar_inverse')) {
    $variables['navbar_classes_array'][] = 'navbar-inverse';
  }
  else {
    $variables['navbar_classes_array'][] = 'navbar-default';
  }
}

/**
 * Implements hook_process_page().
 *
 * @see page.tpl.php
 */
function mne_process_page(&$variables) {
  $variables['navbar_classes'] = implode(' ', $variables['navbar_classes_array']);
}


function mne_preprocess_html(&$variables) {

  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $node = node_load(arg(1));
    if ($node->type == "news" || $node->type == "fotogaleria" || $node->type == "video") {
      $seccion = taxonomy_get_parents($node->field_seccion['und'][0]['tid']);
      if (count($seccion) > 0) {
        foreach ($seccion as $key => $value) {
          $variables['classes_array'][] = "seccion-" . $key;
        }
      }
      else {
        $variables['classes_array'][] = "seccion-" . $node->field_seccion['und'][0]['tid'];
      }
      if ($node->type == "news" && $node->field_especial['und'][0]['value'] == 1) {
        $variables['classes_array'][] = "especiales";
      }
    }
  }

  if (arg(0) == 'taxonomy' && is_numeric(arg(2))) {
    $seccion = taxonomy_get_parents(arg(2));
    if (count($seccion) > 0) {
      foreach ($seccion as $key => $value) {
        $variables['classes_array'][] = "seccion-" . $key;
      }
    }
    else {
      $variables['classes_array'][] = "seccion-" . arg(2);
    }
  }

}



