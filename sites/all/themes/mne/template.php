<?php
/**
 * @file
 * template.php
 */
function taxonomy_node_get_terms($node, $key = 'tid') {
	static $terms;

	if (!isset($terms[$node->vid][$key])) {
		$query = db_select('taxonomy_index', 'r');
		$t_alias = $query->join('taxonomy_term_data', 't', 'r.tid = t.tid');
		$v_alias = $query->join('taxonomy_vocabulary', 'v', 't.vid = v.vid');
		$query->fields( $t_alias );
		$query->condition("r.nid", $node->nid);
		$myvid = 4;
		$query->condition("t.vid", $myvid);
		$result = $query->execute();
		$terms[$node->vid][$key] = array();
		foreach ($result as $term) {
			$terms[$node->vid][$key][$term->$key] = $term;
		}
	}
	return $terms[$node->vid][$key];
}

/**
 * Override or insert variables into the bean templates.
 */
function mne_preprocess_bean(&$variables) {
  // Add a custom class to the bean entity.
  $variables['classes_array'][] = 'publicidad';
}