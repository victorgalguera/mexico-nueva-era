### 1.0-beta2: September 23st, 2015
* Fixes a bug where the slug wasn't taken into account when checking if the url contains the focus keyword.

### 1.0-beta: September 21st, 2015
* Initial beta release