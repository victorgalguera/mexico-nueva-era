# Spanish translation of Simple hierarchical select (7.x-1.6)
# Copyright (c) 2015 by the Spanish translation team
#
msgid ""
msgstr ""
"Project-Id-Version: Simple hierarchical select (7.x-1.6)\n"
"POT-Creation-Date: 2015-05-25 09:05+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: Spanish\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

msgid "Cancel"
msgstr "Cancelar"
msgid "Yes"
msgstr "Sí"
msgid "No"
msgstr "No"
msgid "- None -"
msgstr "- Ninguno -"
msgid "Depth"
msgstr "Profundidad"
msgid "Save"
msgstr "Guardar"
msgid "never"
msgstr "nunca"
msgid "Display number of nodes"
msgstr "Mostrar el numero de nodos"
msgid "Is one of"
msgstr "Es uno de"
msgid "always"
msgstr "siempre"
msgid ""
"The depth will match nodes tagged with terms in the hierarchy. For "
"example, if you have the term \"fruit\" and a child term \"apple\", "
"with a depth of 1 (or higher) then filtering for the term \"fruit\" "
"will get nodes that are tagged with \"apple\" as well as \"fruit\". If "
"negative, the reverse is true; searching for \"apple\" will also pick "
"up nodes tagged with \"fruit\" if depth is -1 (or lower)."
msgstr ""
"La profundidad encajará con los nodos marcados con términos en la "
"jerarquía; por ejemplo: Si tiene el término \"fruta\" y el término "
"hijo \"manzana\" con una profundidad de 1 (o mayor), entonces el "
"filtrado por el término \"fruta\" llegará a los nodos marcados con "
"\"manzana\" además de los de a \"fruta\". Si es negativo, lo "
"contrario también es cierto: La búsqueda de \"manzana\" también "
"alcanzará los términos marcados con \"fruta\" si la profundidad es "
"-1 (o menor)."
msgid "An invalid vocabulary is selected. Please change it in the options."
msgstr ""
"Se ha seleccionado un vocabulario no válido. Por favor, cambielo en "
"las opciones."
msgid "Display the number of nodes associated with the term."
msgstr "Mostrar el numero de nodos asociados al termino"
msgid "Allow creating new terms"
msgstr "Permitir la creación de nuevos terminos"
msgid "Allow creating new levels"
msgstr "Permitir la creación de nuevos niveles"
msgid "Link to term page"
msgstr "Enlazar la pagina del termino"
